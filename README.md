# RPG Heroes


This is .NET assignment given by Noroff. The purpose of the assignment is to get familiar with .NET and C#.

The goal of the assignment is to use plain C# to create a console application with the following minimum requirements.

a) Various hero classes having attributes which increase at different rates as the character gains levels.

Implemented:
- This have been implemented. Through use of inheritance. Hero is here used as an abstract class and all other subclasses (Mage, Ranger, Rougue, Warrior) is inherited the abstract method and using override to increase level at different rates based on the class levelling spesification.
b) Equipment, such as armor and weapons, that characters can equip. The equipped items will alter the power of
the hero, causing it to deal more damage and be able to survive longer. Certain heroes can equip certain item
types.

Implemented:
- Parent class have overloaded methods equip that can take in armor or weapon as the parameters. Each subclass overrides these methods and make sure that certain heroes can equip certain item types.
- TotalAttributes is calculated on the fly base on what item is equipped.
- Damage method return calculated damage combinding weapon damage and totalAttributes. If hero has no weapon equipped it should return 1.

c) Custom exceptions. There are two custom exceptions you are required to write, as detailed in Appendix B.

Implemented:
- Custom exception has been implented but have some minor change from the description.
- InvalidArmorException is triggered when hero wear armor that are not suitable for their class.
- InvalidWeaponException is triggered when hero wear weapon that are not suitable for their class.
- RequiredLevelException is triggered if heroes level is lower than item required level.

d) Testing of the main functionality.
- Have tested all the main functionality in total 28 tests passed.

e) CI pipeline to show that all tests are passed.
- CI pipeline have been included in the gitlab repository.

## Implementation

a) Various hero classes having attributes which increase at different rates as the character gains levels.



b) Equipment, such as armor and weapons, that characters can equip. The equipped items will alter the power of the hero, causing it to deal more damage and be able to survive longer. Certain heroes can equip certain item types.



c) Custom exceptions. There are two custom exceptions you are required to write, as detailed in Appendix B.


## Authors

- [@Chonlawit With-Pettersen](https://gitlab.com/chonlawit.wp/RPG-Heroes)

## Installation

Build any .NET Core sample using the .NET Core CLI, which is installed with the .NET Core SDK. Then run these commands from the CLI in the directory of any sample:

```bash
dotnet build
dotnet run
```
These will install any needed dependencies, build the project, and run the project respectively.

    
## Running Tests

To run tests, run the following command

```bash
  dotnet test RPG_Heroes_Tests/
```

