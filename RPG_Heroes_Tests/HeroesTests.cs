﻿using Microsoft.VisualStudio.TestPlatform.ObjectModel;
using RPG_Heroes.Attributes;
using RPG_Heroes.Characters;
using RPG_Heroes.CustomsExceptions;
using RPG_Heroes.Equipments;

namespace RPG_Heroes_Tests;

public class RPG_Heroes_Tests
{
    #region HeroCreate
    // When a Hero is created, it needs to have the correct name, level, and attributes.
    [Fact]
    public void CreateHero_ShouldHaveCorrectName()
    {
        // Arrange
        Warrior warrior = new Warrior("Chon");
        string expected = "Chon";

        // Act
        string actual = warrior.Name;

        // Assert
        Assert.Equal(expected, actual);
    }

    [Fact]
    public void CreateHero_ShouldHaveCorrectLevel()
    {
        // Arrange
        Ranger ranger = new Ranger("Robin");
        int expected = 1;

        // Act
        int actual = ranger.Level;

        // Assert
        Assert.Equal(expected, actual);
    }

    [Fact]
    public void CreateMage_ShouldHaveCorrectAttributes()
    {
        // Arrange
        Mage mage = new Mage("Mage");
        HeroAttributes expected = new() { Strength = 1, Dexterity = 1, Intelligence = 8 };

        // Act
        HeroAttributes actual = mage.LevelAttributes;

        // Assert
        Assert.Equal(expected, actual);
    }

    [Fact]
    public void CreateRanger_ShouldHaveCorrectAttributes()
    {
        // Arrange
        Ranger ranger = new Ranger("Ranger");
        HeroAttributes expected = new() { Strength = 1, Dexterity = 7, Intelligence = 1 };

        // Act
        HeroAttributes actual = ranger.LevelAttributes;

        // Assert
        Assert.Equal(expected, actual);
    }

    [Fact]
    public void CreateRogue_ShouldHaveCorrectAttributes()
    {
        // Arrange
        Rogue rogue = new Rogue("Rogue");
        HeroAttributes expected = new() { Strength = 2, Dexterity = 6, Intelligence = 1 };

        // Act
        HeroAttributes actual = rogue.LevelAttributes;

        // Assert
        Assert.Equal(expected, actual);
    }

    [Fact]
    public void CreateWarrior_ShouldHaveCorrectAttributes()
    {
        // Arrange
        Warrior warrior = new Warrior("Warrior");
        HeroAttributes expected = new() { Strength = 5, Dexterity = 2, Intelligence = 1 };

        // Act
        HeroAttributes actual = warrior.LevelAttributes;

        // Assert
        Assert.Equal(expected, actual);
    }
    #endregion

    #region HeroLevelling
    [Fact]
    public void LevelUp_ShouldHaveCorrectLevel()
    {
        // Arrange
        Mage mage = new Mage("Mage");
        int expected = 2;


        // Act
        mage.LevelUp(1);
        int actual = mage.Level;

        // Assert
        Assert.Equal(expected, actual);
    }


    [Fact]
    public void LevelUpMage_ShouldHaveCorrectLevelAttributes()
    {
        // Arrange
        Mage mage = new Mage("Mage");
        HeroAttributes expected = new() { Strength = 2, Dexterity = 2, Intelligence = 13 };
        mage.LevelUp(1);


        // Act
        HeroAttributes actual = mage.LevelAttributes;

        // Assert
        Assert.Equal(expected, actual);
    }


    [Fact]
    public void LevelUpRanger_ShouldHaveCorrectLevelAttributes()
    {
        // Arrange
        Ranger ranger = new Ranger("Ranger");
        HeroAttributes expected = new() { Strength = 2, Dexterity = 12, Intelligence = 2 };
        ranger.LevelUp(1);


        // Act
        HeroAttributes actual = ranger.LevelAttributes;

        // Assert
        Assert.Equal(expected, actual);
    }

    [Fact]
    public void LevelUpRogue_ShouldHaveCorrectLevelAttributes()
    {
        // Arrange
        Rogue rogue = new Rogue("Rogue");
        HeroAttributes expected = new() { Strength = 3, Dexterity = 10, Intelligence = 2 };
        rogue.LevelUp(1);


        // Act
        HeroAttributes actual = rogue.LevelAttributes;

        // Assert
        Assert.Equal(expected, actual);
    }

    [Fact]
    public void LevelUpWarrior_ShouldHaveCorrectLevelAttributes()
    {
        // Arrange
        Warrior warrior = new Warrior("Warrior");
        HeroAttributes expected = new() { Strength = 8, Dexterity = 4, Intelligence = 2 };
        warrior.LevelUp(1);


        // Act
        HeroAttributes actual = warrior.LevelAttributes;

        // Assert
        Assert.Equal(expected, actual);
    }

    #endregion

    #region WeaponCreate
    [Fact]
    public void CreateWeapon_ShouldSetCorrectItemNameRequiredLevelSlotWeapontypeDamage()
    {

        Weapon actual = new Weapon("Common axe", 1, WeaponType.Weapon_Axes, 2);

        string itemName = "Common axe";
        int itemRequiredLevel = 1;
        WeaponType weaponType = WeaponType.Weapon_Axes;
        double damage = 2;


        Assert.Equal(itemName, actual.ItemName);
        Assert.Equal(itemRequiredLevel, actual.ItemRequiredLevel);
        Assert.Equal(weaponType, actual.WeaponType);
        Assert.Equal(damage, actual.WeaponDamage);
       
    }
    #endregion

    #region ArmorCreate
    [Fact]
    public void CreateArmor_ShouldSetCorrectItemNameRequiredLevelSlotArmortypeArmorAttributes()
    {

        Armor actual = new Armor("Dragon Plate", 10, Slot.Slot_Body, ArmorType.Armor_Plate, new HeroAttributes() { Strength = 20, Dexterity = 40, Intelligence= 10});

        string itemName = "Dragon Plate";
        int itemRequiredLevel = 10;
        ArmorType armorType = ArmorType.Armor_Plate;
        HeroAttributes armorAttributes = new HeroAttributes() { Strength = 20, Dexterity = 40, Intelligence = 10 };


        Assert.Equal(itemName, actual.ItemName);
        Assert.Equal(itemRequiredLevel, actual.ItemRequiredLevel);
        Assert.Equal(armorType, actual.ArmorType);
        Assert.Equal(armorAttributes, actual.ArmorAttributes);

    }

    #endregion

    #region WeaponEquip

    Weapon axe = new Weapon("Common axe", 1, WeaponType.Weapon_Axes, 2);


    Weapon hammer = new Weapon("Silver hammer", 10, WeaponType.Weapon_Hammars, 50);


    Weapon staff = new Weapon("Common staff", 1, WeaponType.Weapon_Staffs, 2);



    [Fact]
    public void EquipWeapon_HeroEquipValidWeapon_ShouldReturnTrue()
    {
        //Arrange
        Warrior warrior = new("Warrior");
        bool expected = true;

        // Act
        bool actual = warrior.Equip(axe);

        // Assert
        Assert.Equal(expected, actual);
    }

    [Fact]
    public void EquipWeapon_HeroLevelTooLow_ThrowsRequiredLevelException()
    {
        //Arrange
        Warrior warrior = new("Warrior");

        // Act and Assert
        Assert.Throws<RequiredLevelException>(() => warrior.Equip(hammer));
    }

    [Fact]
    public void EquipWeapon_HeroEquipWrongWeapon_ThrowsInvalidWeaponException()
    {
        //Arrange
        Warrior warrior = new("Warrior");

        // Act and Assert
        Assert.Throws<InvalidWeaponException>(() => warrior.Equip(staff));
    }
    #endregion

    #region ArmorEquip
    //Armor_Cloth,
    //Armor_Leather,
    //Armor_Mail,
    //Armor_Plate
    Armor plateBody = new Armor(
        "Common Plate Chest",
        1,
        Slot.Slot_Body,
        ArmorType.Armor_Plate,
        new HeroAttributes() { Strength = 1, Dexterity = 0, Intelligence = 0 }
        );


    Armor leatherPant = new Armor(
        "Dragon Leather pant",
        70,
        Slot.Slot_Legs,
        ArmorType.Armor_Leather,
        new HeroAttributes() { Strength = 10, Dexterity = 50, Intelligence = 10 }
        );



    [Fact]
    public void EquipArmor_HeroEquipValidArmor_ShouldReturnTrue()
    {
        //Arrange
        Warrior warrior = new("Warrior");
        bool expected = true;

        // Act
        bool actual = warrior.Equip(plateBody);

        // Assert
        Assert.Equal(expected, actual);
    }

    [Fact]
    public void EquipArmor_HeroLevelTooLow_ThrowsRequiredLevelException()
    {
        //Arrange
        Warrior warrior = new("Warrior");

        // Act and Assert
        Assert.Throws<RequiredLevelException>(() => warrior.Equip(leatherPant));
    }

    [Fact]
    public void EquipArmor_HeroEquipWrongArmor_ThrowsInvalidWeaponException()
    {
        //Arrange
        Warrior warrior = new("Warrior");
        leatherPant.ItemRequiredLevel = 1;

        // Act and Assert
        Assert.Throws<InvalidArmorExceptions>(() => warrior.Equip(leatherPant));
    }

    #endregion

    #region TotalAttributesCalculation

    Armor headPlate = new Armor(
        "Common head plate",
        1,
        Slot.Slot_Head,
        ArmorType.Armor_Plate,
        new HeroAttributes() { Strength = 0, Dexterity = 0, Intelligence = 1 }
    );

    Armor bodyPlate = new Armor(
        "Common body plate",
        1,
        Slot.Slot_Body,
        ArmorType.Armor_Plate,
        new HeroAttributes() { Strength = 5, Dexterity = 5, Intelligence = 0 }
    );

    Armor bodyMail = new Armor(
        "Common body mail",
        1,
        Slot.Slot_Body,
        ArmorType.Armor_Mail,
        new HeroAttributes() { Strength = 2, Dexterity = 2, Intelligence = 0 }
    );


    [Fact]
    public void TotalAttributesCalculation_WithNoEquipment_ShouldReturnCorrectAttributes()
    {
        //Arrange
        Warrior warrior = new("Warrior");
        HeroAttributes expected = new HeroAttributes() { Strength = 5, Dexterity = 2, Intelligence = 1 };

        // Act
        HeroAttributes actual = warrior.TotalAttributes();

        // Assert
        Assert.Equal(expected, actual);
    }

    [Fact]
    public void TotalAttributesCalculation_WithOneEquipment_ShouldReturnCorrectAttributes()
    {
        //Arrange
        Warrior warrior = new("Warrior");
        warrior.Equip(headPlate);
        HeroAttributes expected = new HeroAttributes() { Strength = 5, Dexterity = 2, Intelligence = 2 };

        // Act
        HeroAttributes actual = warrior.TotalAttributes();

        // Assert
        Assert.Equal(expected, actual);
    }

    [Fact]
    public void TotalAttributesCalculation_WithTwoEquipment_ShouldReturnCorrectAttributes()
    {
        //Arrange
        Warrior warrior = new("Warrior");
        warrior.Equip(headPlate);
        warrior.Equip(bodyPlate);
        HeroAttributes expected = new HeroAttributes() { Strength = 10, Dexterity = 7, Intelligence = 2 };

        // Act
        HeroAttributes actual = warrior.TotalAttributes();

        // Assert
        Assert.Equal(expected, actual);
    }

    [Fact]
    public void TotalAttributesCalculation_ReplaceBodyPlateWithBodyMail_ShouldReturnCorrectAttributes()
    {
        //Arrange
        Warrior warrior = new("Warrior");
        warrior.Equip(bodyPlate);
        warrior.Equip(bodyMail);
        HeroAttributes expected = new HeroAttributes() { Strength = 7, Dexterity = 4, Intelligence = 1 };

        // Act
        HeroAttributes actual = warrior.TotalAttributes();

        // Assert
        Assert.Equal(expected, actual);
    }
    #endregion

    #region DamageCalculation
    [Fact]
    public void DamageCalculation_NoWeaponEquipped_ShouldCorrectCalculatedDamage()
    {
        //Arrange
        Warrior warrior = new("Warrior");
        double expected = 1 * (1 + (5 / 100));

        // Act
        double actual = warrior.Damage();

        // Assert
        Assert.Equal(expected, actual);
    }

    [Fact]
    public void DamageCalculation_WithCommmonAxeEquipped_ShouldReturnCorrectCalculatedDamage()
    {
        //Arrange
        Warrior warrior = new("Warrior");
        double expected = 2 * (1 + (5 / 100));

        // Act
        warrior.Equip(axe);
        double actual = warrior.Damage();

        // Assert
        Assert.Equal(expected, actual);
    }

    [Fact]
    public void DamageCalculation_ReplaceCommonAxeWithSilverHammer_ShouldReturnCorrectCalculatedDamage()
    {
        //Arrange
        Warrior warrior = new("Warrior");
        double expected = 50 * (1 + (5 / 100));

        // Act
        warrior.Equip(axe);
        warrior.LevelUp(10);
        warrior.Equip(hammer);
        double actual = warrior.Damage();

        // Assert
        Assert.Equal(expected, actual);
    }

    [Fact]
    public void DamageCalculation_WithCommonAxeAndCommonPlateChest_ShouldReturnCorrectCalculatedDamage()
    {
        //Arrange
        Warrior warrior = new("Warrior");
        double expected = 2 * (1 + ((5 + 1) / 100));

        // Act
        warrior.Equip(axe);
        warrior.Equip(plateBody);
        double actual = warrior.Damage();

        // Assert
        Assert.Equal(expected, actual);
    }
    #endregion

    #region Display
    [Fact]
    public void Display_ShouldDisplayHeroDetailsCorrectly()
    {
        // Arrange
        Warrior warrior = new("Chon");

        string expected =
            $"Name: Chon\n" +
            $"Class: Warrior\n" +
            $"Level: 1\n" +
            $"Total strength: 5\n" +
            $"Total dexterity: 2\n" +
            $"Total intelligence: 1\n" +
            $"Damage: 1\n";

        // Act
        string name = warrior.Name;
        string className = warrior.GetType().Name;
        int level = warrior.Level;
        HeroAttributes attributes = warrior.LevelAttributes;
        double damage = warrior.Damage();

        string actual = warrior.Display(name, className, level, attributes, damage);

        // Assert
        Assert.Equal(expected, actual);
    }
    #endregion

}
