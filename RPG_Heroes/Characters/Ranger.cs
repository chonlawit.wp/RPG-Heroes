﻿using System;
using RPG_Heroes.Attributes;
using RPG_Heroes.CustomsExceptions;
using RPG_Heroes.Equipments;

namespace RPG_Heroes.Characters
{
    public class Ranger : Hero
    {
        public Ranger(string name) : base(name, 1, 7, 1)
        {
            ValidWeaponTypes.Add(WeaponType.Weapon_Bows);

            ValidArmorTypes.Add(ArmorType.Armor_Leather);
            ValidArmorTypes.Add(ArmorType.Armor_Mail);

        }

        public override double Damage()
        {
            HeroAttributes DamagingAttribute = TotalAttributes();

            bool hasWeapon = Equipment.TryGetValue(Slot.Slot_Weapon, out Item weapon);
            if (hasWeapon)
            {
                Weapon w = (Weapon)weapon;
                double heroDamge = w.WeaponDamage * (1 + DamagingAttribute.Dexterity / 100);
                return heroDamge;
            }
            else
            {
                return 1;
            }
        }



        public override bool Equip(Weapon weapon)
        {
            if (weapon.ItemRequiredLevel > Level)
            {
                throw new RequiredLevelException($"You needs to reach level {weapon.ItemRequiredLevel} to equip this weapon");
            }

            if (!ValidWeaponTypes.Contains(weapon.WeaponType))
            {
                throw new InvalidWeaponException($"{weapon.WeaponType} is not wearable by ranger");
            }


            Equipment[weapon.ItemSlot] = weapon;

            bool equipped = Equipment.ContainsValue(weapon);
            if (equipped)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        public override bool Equip(Armor armor)
        {
            if (armor.ItemRequiredLevel > Level)
            {
                throw new RequiredLevelException($"You needs to reach level {armor.ItemRequiredLevel} to equip this armor");
            }

            if (!ValidArmorTypes.Contains(armor.ArmorType))
            {
                throw new InvalidArmorExceptions($"{armor.ArmorType} is not wearable by rangers");
            }

            Equipment[armor.ItemSlot] = armor;

            bool equipped = Equipment.ContainsValue(armor);
            if (equipped)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        public override void LevelUp(int level)
        {
            HeroAttributes levelingUp = new() { Strength = level * 1, Dexterity = level * 5, Intelligence = level * 1 };
            LevelAttributes += levelingUp;

            Level += level;
        }
    }
}

