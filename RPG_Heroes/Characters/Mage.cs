﻿using System;
using RPG_Heroes.Attributes;
using RPG_Heroes.CustomsExceptions;
using RPG_Heroes.Equipments;

namespace RPG_Heroes.Characters
{
    public class Mage : Hero
    {
        public Mage(string name) : base(name, 1, 1, 8)
        {
            ValidWeaponTypes.Add(WeaponType.Weapon_Staffs);
            ValidWeaponTypes.Add(WeaponType.Weapon_Wands);

            ValidArmorTypes.Add(ArmorType.Armor_Cloth);

        }




        public override double Damage()
        { 
            //Damaging attribute does not need to be stored, it can just be calculated from TotalAttributes
            HeroAttributes DamagingAttribute = TotalAttributes();


            bool hasWeapon = Equipment.TryGetValue(Slot.Slot_Weapon, out Item weapon);
            if (hasWeapon)
            {
                //Hero damge = WeaponDamage * (1 + DamagingAttribute/100)
                Weapon w = (Weapon)weapon;
                double heroDamge = w.WeaponDamage * (1 + DamagingAttribute.Intelligence / 100);
                return heroDamge;
            }
            else
            {
                //If a Hero has no weapon equipped, take their WeaponDamage to be 1.
                return 1;
            }

        }




        public override bool Equip(Weapon weapon)
        {
            if(weapon.ItemRequiredLevel > Level)
            {
                throw new RequiredLevelException($"You needs to reach level {weapon.ItemRequiredLevel} to equip this weapon");
            }

            if (!ValidWeaponTypes.Contains(weapon.WeaponType))
            {
                throw new InvalidWeaponException($"{weapon.WeaponType} is not wearable by mage");
            }
             

            Equipment[weapon.ItemSlot] = weapon;

            bool equipped = Equipment.ContainsValue(weapon);
            if (equipped)
            {
                return true;
            }
            else
            {
                return false;
            }

           
        }





        public override bool Equip(Armor armor)
        {
            if(armor.ItemRequiredLevel > Level)
            {
                throw new RequiredLevelException($"You needs to reach level {armor.ItemRequiredLevel} to equip this armor");
            }

            if(!ValidArmorTypes.Contains(armor.ArmorType))
            {
                throw new InvalidArmorExceptions($"{armor.ArmorType} is not wearable by mages");
            }

            Equipment[armor.ItemSlot] = armor;

            bool equipped = Equipment.ContainsValue(armor);
            if (equipped)
            {
                return true;
            }
            else
            {
                return false;
            }
        }




        public override void LevelUp(int level)
        {
            HeroAttributes levelingUp = new() { Strength = level * 1, Dexterity = level * 1, Intelligence = level * 5 };
            LevelAttributes += levelingUp;

            Level += level;
        }
    }
}

