﻿using System;
using System.Text;
using RPG_Heroes.Attributes;
using RPG_Heroes.Equipments;


namespace RPG_Heroes.Characters
{
    public abstract class Hero
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public HeroAttributes LevelAttributes { get; set; }
        public Dictionary<Slot, Item> Equipment { get; set; }
        public List<WeaponType> ValidWeaponTypes { get; set; }
        public List<ArmorType> ValidArmorTypes { get; set; }



        public Hero(string name, int strength, int dexterity, int intelligence)
        {
            Name = name;
            Level = 1;
            LevelAttributes = new HeroAttributes()
            {
                Strength = strength,
                Dexterity = dexterity,
                Intelligence = intelligence
            };
            Equipment = new Dictionary<Slot, Item>();
            ValidWeaponTypes = new List<WeaponType>();
            ValidArmorTypes = new List<ArmorType>();
        }


        // LevelUp - increases the level of a character by 1 and increases their LevelAttributes.
        public abstract void LevelUp(int level);


        // Equip Overloaded - Equipping weapons 
        public abstract bool Equip(Weapon weapon);

        // Equip Overloaded - Equipping armor
        public abstract bool Equip(Armor armor);

        // Damage - damage is calculated on the fly and not stored
        public abstract double Damage();

    



        // TotalAttributes – calculated on the fly and not stored
        public HeroAttributes TotalAttributes()
        {

            bool hasHeadArmor = Equipment.TryGetValue(Slot.Slot_Head, out Item headArmor);
            bool hasBodyArmor = Equipment.TryGetValue(Slot.Slot_Body, out Item bodyArmor);
            bool hasLegsArmor = Equipment.TryGetValue(Slot.Slot_Legs, out Item legsArmor);

            HeroAttributes totalArmorAttributes = new() { Strength = 0, Dexterity = 0, Intelligence = 0 };
            
            if (hasHeadArmor)
            {
                Armor head = (Armor)headArmor;
                totalArmorAttributes += new HeroAttributes() { Strength = head.ArmorAttributes.Strength, Dexterity = head.ArmorAttributes.Dexterity, Intelligence = head.ArmorAttributes.Intelligence };
            }

            if (hasBodyArmor)
            {
                Armor body = (Armor)bodyArmor;
                totalArmorAttributes += new HeroAttributes() { Strength = body.ArmorAttributes.Strength, Dexterity = body.ArmorAttributes.Dexterity, Intelligence = body.ArmorAttributes.Intelligence };
            }

            if(hasLegsArmor)
            {
                Armor legs = (Armor)legsArmor;
                totalArmorAttributes += new HeroAttributes() { Strength = legs.ArmorAttributes.Strength, Dexterity = legs.ArmorAttributes.Dexterity, Intelligence = legs.ArmorAttributes.Intelligence };
            }


            //Total = LevelAttributes + (Sum of ArmorAttribute for all Armor in Equipment)
            HeroAttributes Total = LevelAttributes + totalArmorAttributes;


            return Total;
            
        }



        // Display – details of Hero to be displayed
        public string Display(string name, string className, int level, HeroAttributes attribute, double damage)
        {
            StringBuilder heroDetails = new StringBuilder();
            heroDetails.AppendFormat($"Name: {name}\n");
            heroDetails.AppendFormat($"Class: {className}\n");
            heroDetails.AppendFormat($"Level: {level}\n");
            heroDetails.AppendFormat($"Total strength: {attribute.Strength}\n");
            heroDetails.AppendFormat($"Total dexterity: {attribute.Dexterity}\n");
            heroDetails.AppendFormat($"Total intelligence: {attribute.Intelligence}\n");
            heroDetails.AppendFormat($"Damage: {damage}\n");


            return heroDetails.ToString();
        }

    }
}

