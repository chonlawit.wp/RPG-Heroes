﻿using System;
using RPG_Heroes.Attributes;
using RPG_Heroes.CustomsExceptions;
using RPG_Heroes.Equipments;

namespace RPG_Heroes.Characters
{
    public class Warrior : Hero
    {
        public Warrior(string name) : base(name, 5, 2, 1)
        {
            ValidWeaponTypes.Add(WeaponType.Weapon_Axes);
            ValidWeaponTypes.Add(WeaponType.Weapon_Hammars);
            ValidWeaponTypes.Add(WeaponType.Weapon_Swords);

            ValidArmorTypes.Add(ArmorType.Armor_Mail);
            ValidArmorTypes.Add(ArmorType.Armor_Plate);

        }

        public override double Damage()
        {
            HeroAttributes DamagingAttribute = TotalAttributes();

            bool hasWeapon = Equipment.TryGetValue(Slot.Slot_Weapon, out Item weapon);
            if (hasWeapon)
            {
                Weapon w = (Weapon)weapon;
                double heroDamge = w.WeaponDamage * (1 + DamagingAttribute.Strength / 100);
                return heroDamge;
            }
            else
            {
                return 1;
            }
        }

        public override bool Equip(Weapon weapon)
        {
            if (weapon.ItemRequiredLevel > Level)
            {
                throw new RequiredLevelException($"You needs to reach level {weapon.ItemRequiredLevel} to equip this weapon");
            }


            if (!ValidWeaponTypes.Contains(weapon.WeaponType))
            {
                throw new InvalidWeaponException($"{weapon.WeaponType} is not wearable by warrior");
            }


            Equipment[weapon.ItemSlot] = weapon;

            bool equipped = Equipment.ContainsValue(weapon);
            if (equipped)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override bool Equip(Armor armor)
        {
            if (armor.ItemRequiredLevel > Level)
            {
                throw new RequiredLevelException($"You needs to reach level {armor.ItemRequiredLevel} to equip this armor");
            }

            if (!ValidArmorTypes.Contains(armor.ArmorType))
            {
                throw new InvalidArmorExceptions($"{armor.ArmorType} is not wearable by warriors");
            }

            Equipment[armor.ItemSlot] = armor;

            bool equipped = Equipment.ContainsValue(armor);
            if (equipped)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override void LevelUp(int level)
        {
            HeroAttributes levelingUp = new() { Strength = level * 3, Dexterity = level * 2, Intelligence = level * 1 };
            LevelAttributes += levelingUp;

            Level += level;
        }
    }
}

