﻿using System;
namespace RPG_Heroes.Attributes
{
    public class HeroAttributes
    {

        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        public override bool Equals(object? obj)
        {
            return obj is HeroAttributes attributes &&
                   Strength == attributes.Strength &&
                   Dexterity == attributes.Dexterity &&
                   Intelligence == attributes.Intelligence;
        }


        public static HeroAttributes operator +(HeroAttributes lhs, HeroAttributes rhs)
        {
            return new HeroAttributes
            {
                Strength = lhs.Strength + rhs.Strength,
                Dexterity = lhs.Dexterity + rhs.Dexterity,
                Intelligence = lhs.Intelligence + rhs.Intelligence
            };
        }
    }
}

