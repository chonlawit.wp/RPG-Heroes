﻿using System;
namespace RPG_Heroes.CustomsExceptions
{
    public class InvalidArmorExceptions : Exception
    {
        public InvalidArmorExceptions()
        {
        }

        public InvalidArmorExceptions(string? message) : base(message)
        {
        }

        public InvalidArmorExceptions(string? message, Exception? innerException) : base(message, innerException)
        {
        }

        public override string Message => "InvalidArmorException";
    }
}

