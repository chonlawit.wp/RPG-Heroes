﻿using System;
namespace RPG_Heroes.CustomsExceptions
{
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException()
        {
        }

        public InvalidWeaponException(string? message) : base(message)
        {
        }

        public InvalidWeaponException(string? message, Exception? innerException) : base(message, innerException)
        {
        }

        public override string Message => "InvalidWeaponException";
    }
}

