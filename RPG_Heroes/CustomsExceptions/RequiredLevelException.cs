﻿using System;
namespace RPG_Heroes.CustomsExceptions
{
    public class RequiredLevelException : Exception
    {
        public RequiredLevelException()
        {
        }

        public RequiredLevelException(string? message) : base(message)
        {
        }

        public RequiredLevelException(string? message, Exception? innerException) : base(message, innerException)
        {
        }

        public override string Message => "RequiredLevelException";
    }
}

