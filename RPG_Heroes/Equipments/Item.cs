﻿using System;
namespace RPG_Heroes.Equipments
{
    public enum Slot
    {
        Slot_Weapon,
        Slot_Head,
        Slot_Body,
        Slot_Legs      
    }

    public abstract class Item
    {

        public string ItemName { get; set; }
        public int ItemRequiredLevel { get; set; }
        public Slot ItemSlot { get; set; }

        public Item(string itemName, int itemRequiredLevel, Slot itemSlot)
        {
            ItemName = itemName;
            ItemRequiredLevel = itemRequiredLevel;
            ItemSlot = itemSlot;

        }

    
       
    }
}

