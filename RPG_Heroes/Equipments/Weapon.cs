﻿using System;
using System.Text;

namespace RPG_Heroes.Equipments
{
    public enum WeaponType
    {
        Weapon_Axes,
        Weapon_Bows,
        Weapon_Daggers,
        Weapon_Hammars,
        Weapon_Staffs,
        Weapon_Swords,
        Weapon_Wands

    }
    public class Weapon : Item
    {
        public WeaponType WeaponType { get; set; }
        public double WeaponDamage { get; set; }

        //When a weapon is created, it is automatically given the Slot of Weapon.
        public Weapon(string itemName, int requiredLevel, WeaponType weaponType, double weaponDamage) : base(itemName, requiredLevel, 0) 
        {
            WeaponType = weaponType;
            WeaponDamage = weaponDamage;
        }

    }
}

