﻿using System;
using RPG_Heroes.Attributes;

namespace RPG_Heroes.Equipments
{
    public enum ArmorType
    {
        Armor_Cloth,
        Armor_Leather,
        Armor_Mail,
        Armor_Plate
    }

    public class Armor : Item
    {
        public ArmorType ArmorType { get; set; }
        public HeroAttributes ArmorAttributes { get; set; }

        public Armor(string itemName, int requiredLevel, Slot itemSlot, ArmorType armorType, HeroAttributes armorAttributes) : base(itemName, requiredLevel, itemSlot)
        {
            ArmorType = armorType;
            ArmorAttributes = armorAttributes;
            
        }

       
    }
}

