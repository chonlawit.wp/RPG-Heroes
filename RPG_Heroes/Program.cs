﻿
using System.Reflection.Emit;
using System.Xml.Linq;
using RPG_Heroes.Attributes;
using RPG_Heroes.Characters;
using static System.Net.Mime.MediaTypeNames;


Warrior warrior = new("Chon");
string name = warrior.Name;
string className = warrior.GetType().Name;
int level = warrior.Level;
HeroAttributes attributes = warrior.LevelAttributes;
double damage = warrior.Damage();

string result = warrior.Display(name, className, level, attributes, damage);

Console.Write(result);


